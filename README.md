# Portfolio Manager

# Code quality
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=rjrodrigues_portfolio-manager&metric=alert_status)](https://sonarcloud.io/dashboard?id=rjrodrigues_portfolio-manager)

Create library domain
    - Move Dto's and entities there
    - Where do we write our exceptions?
    
Create library infrastructure
    - Logger
    
Where do we write converters?

Write tests for our REST controllers
    - where we test:
        - 200
        - 201
        - 401
        - 403
        - 404
        - 409
    - we use Mock Data
    
Do we use Response Entity?
Do we even use Spring Boot for REST? Or jersey?

Exception handling -> Http Code











JBOSS deployment
 