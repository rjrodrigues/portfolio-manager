package com.dfse.portfoliomanager.domain.interfaces;

import com.dfse.portfoliomanager.domain.entities.User;

import java.util.List;

public interface IUserService {
    List<User> getUsers();

    User getUser(long id);

    long createUser(User user);

    long deleteUser(long id);
}
