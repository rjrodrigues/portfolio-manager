package com.dfse.portfoliomanager.domain.entities;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AuthLogin {
    private String email;
    private String password;
}
