package com.dfse.portfoliomanager.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private LocalDateTime creationDate;
    private LocalDateTime modificationDate;
}
