package com.dfse.portfoliomanager.domain.interfaces;

import com.dfse.portfoliomanager.domain.entities.AuthLogin;

public interface IAuthenticationService {
    String login(AuthLogin authLogin);
}
