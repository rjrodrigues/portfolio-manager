package com.dfse.portfoliomanager.domain.messages;

import lombok.Data;

@Data
public class MessageHeader {
    private String version;
    private String message;
    private Object data;
}
