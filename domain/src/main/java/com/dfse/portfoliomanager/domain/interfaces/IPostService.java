package com.dfse.portfoliomanager.domain.interfaces;

import com.dfse.portfoliomanager.domain.entities.Post;

import java.util.List;

public interface IPostService {
    List<Post> getPosts();

    List<Post> refreshCache();
}
