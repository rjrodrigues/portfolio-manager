package com.dfse.portfoliomanager.domain.interfaces;

import com.dfse.portfoliomanager.domain.entities.Portfolio;

import java.util.List;

public interface IPortfolioService {
    List<Portfolio> getPortfolios();

    Portfolio getPortfolio(long id);

    long createPortfolio(Portfolio portfolio);

    long deletePortfolio(long id);
}
