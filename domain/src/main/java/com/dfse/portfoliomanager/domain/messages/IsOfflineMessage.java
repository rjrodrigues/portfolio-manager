package com.dfse.portfoliomanager.domain.messages;

import lombok.Data;

@Data
public class IsOfflineMessage {
    private boolean isOffline;
}
