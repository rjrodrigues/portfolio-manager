package com.dfse.portfoliomanager.domain.interfaces;

public interface ILogger {
    public void info(String message, Object object);

    public void debug(String message, Object object);

    public void warn(String message, Object object);

    public void error(String message, Object object);
}
