package com.dfse.portfoliomanager.presentation.converter;

public interface IDtoToEntityConverter<T, S> {
    T convertToEntity(S source);
}
