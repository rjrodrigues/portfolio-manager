package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.domain.entities.Portfolio;
import com.dfse.portfoliomanager.domain.interfaces.IPortfolioService;
import com.dfse.portfoliomanager.domain.interfaces.IPostService;
import com.dfse.portfoliomanager.presentation.converter.PortfolioConverter;
import com.dfse.portfoliomanager.presentation.converter.PostConverter;
import com.dfse.portfoliomanager.presentation.dto.PortfolioDto;
import com.dfse.portfoliomanager.presentation.dto.PostDto;
import com.dfse.portfoliomanager.presentation.model.UserRequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/portfolios")
@Slf4j
public class PortfolioController {

    private final IPortfolioService portfolioService;
    private final PortfolioConverter portfolioConverter;
    private final IPostService postService;
    private final PostConverter postConverter;

    @Resource(name = "userRequestContextBean")
    UserRequestContext userRequestContext;

    @Autowired
    public PortfolioController(IPortfolioService portfolioService, IPostService postService) {
        this.portfolioService = portfolioService;
        this.postService = postService;
        portfolioConverter = new PortfolioConverter();
        postConverter = new PostConverter();
    }

    @GetMapping
    public List<PortfolioDto> getPortfolios() {
        System.out.println("GET Portfolios");
        System.out.println(userRequestContext.getToken());
        return this.portfolioService.getPortfolios()
                .stream()
                .map(portfolioConverter::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PortfolioDto> getPortfolio(@PathVariable long id) {
        Portfolio portfolio = portfolioService.getPortfolio(id);
        return new ResponseEntity<>(portfolioConverter.convertToDto(portfolio), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Long> createPortfolio(@RequestBody PortfolioDto portfolioDto) {
        return new ResponseEntity<>(portfolioService.createPortfolio(portfolioConverter.convertToEntity(portfolioDto)), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deletePortfolio(@PathVariable long id) {
        return new ResponseEntity<>(portfolioService.deletePortfolio(id), HttpStatus.OK);
    }

    @GetMapping("/posts")
    public List<PostDto> getPosts() {
        return postService.getPosts().stream().map(postConverter::convertToDto).collect(Collectors.toList());
    }
}
