package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.domain.interfaces.IUserService;
import com.dfse.portfoliomanager.presentation.converter.UserConverter;
import com.dfse.portfoliomanager.presentation.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final IUserService userService;
    private final UserConverter userConverter;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
        userConverter = new UserConverter();
    }

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers().stream().map(userConverter::convertToDto).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable long id) {
        return new ResponseEntity<>(userConverter.convertToDto(userService.getUser(id)), HttpStatus.OK);
    }
}
