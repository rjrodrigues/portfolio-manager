package com.dfse.portfoliomanager.presentation.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI customOpenAPI(@Value("${application-description}") String appDescription,
                                 @Value("${application-version}") String appVersion) {
        System.out.println("open api");
        return new OpenAPI().info(new Info()
                .title("Portfolio Manager API")
                .version(appVersion)
                .description(appDescription));
    }
}
