package com.dfse.portfoliomanager.presentation.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessage {
    private String username;
    private String message;
}
