package com.dfse.portfoliomanager.presentation.controller.common;

import com.dfse.portfoliomanager.infrastructure.exceptions.EmailOrPasswordIncorrectException;
import com.dfse.portfoliomanager.infrastructure.exceptions.UserTemporaryBlockedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AuthenticationControllerExceptionAdvice {

    private static final String EMAIL_OR_PASSWORD_INCORRECT = "error.emailOrPasswordIncorrect";
    private static final String USER_BLOCKED = "error.userBlocked";

    @ExceptionHandler(value = EmailOrPasswordIncorrectException.class)
    public ResponseEntity<CustomErrorResponse> handleEmailOrPasswordIncorrectException(EmailOrPasswordIncorrectException e) {
        CustomErrorResponse error = new CustomErrorResponse(EMAIL_OR_PASSWORD_INCORRECT, e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = UserTemporaryBlockedException.class)
    public ResponseEntity<CustomErrorResponse> handleUserTemporaryBlockedException(UserTemporaryBlockedException e) {
        CustomErrorResponse error = new CustomErrorResponse(USER_BLOCKED, e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }
}
