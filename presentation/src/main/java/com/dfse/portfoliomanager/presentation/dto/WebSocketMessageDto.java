package com.dfse.portfoliomanager.presentation.dto;

import lombok.Getter;

@Getter
public class WebSocketMessageDto {
    public String sender;
    public String message;
}
