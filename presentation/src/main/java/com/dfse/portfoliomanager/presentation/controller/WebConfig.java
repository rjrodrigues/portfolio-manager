package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.presentation.model.UserRequestContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Bean
    @RequestScope
    public UserRequestContext userRequestContextBean() {
        return new UserRequestContext("123123123123123");
    }

}
