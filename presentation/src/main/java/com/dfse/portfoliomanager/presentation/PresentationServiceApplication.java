package com.dfse.portfoliomanager.presentation;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {
        "com.dfse.portfoliomanager.presentation.controller",
        //"com.dfse.portfoliomanager.presentation.filter",
        "com.dfse.portfoliomanager.infrastructure.service"
})
@EnableJpaRepositories(
        basePackages = {"com.dfse.portfoliomanager.database.*"}
)
@EntityScan("com.dfse.portfoliomanager.domain.entities")
@EnableFeignClients(basePackages = "com.dfse.portfoliomanager.infrastructure.feign")
@EnableCaching
@EnableScheduling
public class PresentationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PresentationServiceApplication.class, args);
    }

    @Bean
    public SocketIOServer socketIOServer() {
        Configuration config = new Configuration();
        config.setHostname("localhost");
        config.setPort(9092);
        return new SocketIOServer(config);
    }

//    @Bean
//    public FilterRegistrationBean<RequestLoggingFilter> loggingFilter() {
//        FilterRegistrationBean<RequestLoggingFilter> registrationBean = new FilterRegistrationBean<>();
//        registrationBean.setFilter(new RequestLoggingFilter());
//        registrationBean.addUrlPatterns("/api/portfolios");
//        // registrationBean.setUrlPatterns(Collections.singletonList("/api/*"));
//        return registrationBean;
//    }

//    @Bean
//    @RequestScope
//    public UserRequestContext userRequestContextBean() {
//        return new UserRequestContext();
//    }
}
