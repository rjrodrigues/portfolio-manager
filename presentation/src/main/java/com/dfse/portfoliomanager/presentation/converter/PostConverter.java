package com.dfse.portfoliomanager.presentation.converter;

import com.dfse.portfoliomanager.domain.entities.Post;
import com.dfse.portfoliomanager.presentation.dto.PostDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service
@Qualifier("PostConverter")
public class PostConverter implements IEntityToDtoConverter<PostDto, Post>, IDtoToEntityConverter<Post, PostDto> {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Override
    public PostDto convertToDto(Post post) {
        return modelMapper.map(post, PostDto.class);
    }

    @Override
    public Post convertToEntity(PostDto postDto) {
        return modelMapper.map(postDto, Post.class);
    }

}
