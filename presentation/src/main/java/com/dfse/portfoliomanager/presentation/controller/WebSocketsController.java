package com.dfse.portfoliomanager.presentation.controller;

import com.corundumstudio.socketio.HandshakeData;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.dfse.portfoliomanager.presentation.model.ChatMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class WebSocketsController {

    private final SocketIOServer server;
    // private final SocketIONamespace namespace;

    @Autowired
    public WebSocketsController(SocketIOServer server) {
        this.server = server;
        this.server.addConnectListener(onConnected());
        this.server.addDisconnectListener(onDisconnected());
        this.server.addEventListener("chat", ChatMessage.class, onChatReceived());
        // this.namespace = server.addNamespace("/chat");
        // this.namespace.addConnectListener(onConnected());
        // this.namespace.addDisconnectListener(onDisconnected());
        // this.namespace.addEventListener("chat", ChatMessage.class, onChatReceived());
        this.server.start();
    }

    private DataListener<ChatMessage> onChatReceived() {
        return (client, data, ackSender) -> {
            System.out.println("Client[{}] - Received chat message '{}'" + client.getSessionId().toString() + data);
            server.getBroadcastOperations().sendEvent("chat", data);
        };
    }

    private ConnectListener onConnected() {
        return client -> {
            HandshakeData handshakeData = client.getHandshakeData();
            System.out.println("Client[{}] - Connected to chat module through '{}'" + client.getSessionId().toString() + handshakeData.getUrl());
            client.joinRoom("private");
        };
    }

    private DisconnectListener onDisconnected() {
        return client -> {
            System.out.println("Client[{}] - Disconnected from chat module." + client.getSessionId().toString());
        };
    }

    @Scheduled(fixedRate = 5000)
    private void sendMessage() {
        server.getBroadcastOperations().sendEvent("event");
    }

    @Scheduled(fixedRate = 1000)
    private void sendPrivateMessage() {
        server.getRoomOperations("private").sendEvent("privateEvent");
    }
}
