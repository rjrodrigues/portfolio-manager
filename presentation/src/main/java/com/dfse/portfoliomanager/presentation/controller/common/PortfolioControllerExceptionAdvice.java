package com.dfse.portfoliomanager.presentation.controller.common;

import com.dfse.portfoliomanager.infrastructure.exceptions.PortfolioNameAlreadyExistsException;
import com.dfse.portfoliomanager.infrastructure.exceptions.PortfolioNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class PortfolioControllerExceptionAdvice {

    private static final String PORTFOLIO_NOT_FOUND_MESSAGE = "error.portfolioNotFound";
    private static final String PORTFOLIO_NAME_ALREADY_EXISTS_MESSAGE = "error.portfolioNameAlreadyExists";

    @ExceptionHandler(value = PortfolioNotFoundException.class)
    public ResponseEntity<CustomErrorResponse> handlePortfolioNotFoundException(PortfolioNotFoundException e) {
        CustomErrorResponse error = new CustomErrorResponse(PORTFOLIO_NOT_FOUND_MESSAGE, e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = PortfolioNameAlreadyExistsException.class)
    public ResponseEntity<CustomErrorResponse> handlePortfolioNameAlreadyExistsException(PortfolioNameAlreadyExistsException e) {
        CustomErrorResponse error = new CustomErrorResponse(PORTFOLIO_NAME_ALREADY_EXISTS_MESSAGE, e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }
}
