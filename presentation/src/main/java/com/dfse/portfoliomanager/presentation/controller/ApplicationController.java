package com.dfse.portfoliomanager.presentation.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApplicationController {

    @GetMapping
    public String getApplicationInfo() {
        return "Portfolio Manager";
    }

    @GetMapping("/version")
    public String getVersion() {
        return "0.0.1";
    }
}
