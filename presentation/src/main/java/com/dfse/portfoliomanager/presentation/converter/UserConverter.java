package com.dfse.portfoliomanager.presentation.converter;

import com.dfse.portfoliomanager.domain.entities.User;
import com.dfse.portfoliomanager.presentation.dto.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("UserConverter")
public class UserConverter implements IEntityToDtoConverter<UserDto, User>, IDtoToEntityConverter<User, UserDto> {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Override
    public UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public User convertToEntity(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }

}
