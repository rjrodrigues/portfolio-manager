package com.dfse.portfoliomanager.presentation.converter;

import com.dfse.portfoliomanager.domain.entities.Portfolio;
import com.dfse.portfoliomanager.presentation.dto.PortfolioDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("PortfolioConverter")
public class PortfolioConverter implements IEntityToDtoConverter<PortfolioDto, Portfolio>, IDtoToEntityConverter<Portfolio, PortfolioDto> {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Override
    public PortfolioDto convertToDto(Portfolio portfolio) {
        return modelMapper.map(portfolio, PortfolioDto.class);
    }

    @Override
    public Portfolio convertToEntity(PortfolioDto portfolioDto) {
        return modelMapper.map(portfolioDto, Portfolio.class);
    }

}
