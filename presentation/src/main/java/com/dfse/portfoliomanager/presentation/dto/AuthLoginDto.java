package com.dfse.portfoliomanager.presentation.dto;

import lombok.Data;

@Data
public class AuthLoginDto {
    private String email;
    private String password;
}
