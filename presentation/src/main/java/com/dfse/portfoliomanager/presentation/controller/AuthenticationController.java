package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.domain.entities.AuthLogin;
import com.dfse.portfoliomanager.domain.interfaces.IAuthenticationService;
import com.dfse.portfoliomanager.presentation.dto.AuthLoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final IAuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(IAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping
    public ResponseEntity<String> login(@RequestBody AuthLoginDto loginDto) {
        AuthLogin authLogin = new AuthLogin();
        authLogin.setEmail(loginDto.getEmail());
        authLogin.setPassword(loginDto.getPassword());
        return new ResponseEntity<>(authenticationService.login(authLogin), HttpStatus.OK);
    }

    // @PostMapping
    // public ResponseEntity register() {
    // ?? already exists -> code 409
    // }
}
