package com.dfse.portfoliomanager.presentation.converter;

public interface IEntityToDtoConverter<T, S> {
    T convertToDto(S source);
}
