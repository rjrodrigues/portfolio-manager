package com.dfse.portfoliomanager.presentation.controller.common;

import com.dfse.portfoliomanager.infrastructure.exceptions.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UserControllerExceptionAdvice {

    private static final String USER_NOT_FOUND_MESSAGE = "error.userNotFound";

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<CustomErrorResponse> handleUserNotFoundException(UserNotFoundException e) {
        CustomErrorResponse error = new CustomErrorResponse(USER_NOT_FOUND_MESSAGE, e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
