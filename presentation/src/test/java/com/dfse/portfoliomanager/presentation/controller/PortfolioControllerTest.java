package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.presentation.controller.common.PortfolioControllerExceptionAdvice;
import com.dfse.portfoliomanager.presentation.mockservices.MockPortfolioService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {PortfolioController.class, PortfolioControllerExceptionAdvice.class, MockPortfolioService.class})
@WebMvcTest
class PortfolioControllerTest {

    private final String portfoliosBaseUrl = "/api/portfolios";
    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetPortfoliosSuccessful() throws Exception {
        mockMvc.perform(get(portfoliosBaseUrl))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(
                        "[" +
                                "{\"id\":0,\"name\":\"Name_0\"}," +
                                "{\"id\":1,\"name\":\"Name_1\"}," +
                                "{\"id\":2,\"name\":\"Name_2\"}," +
                                "{\"id\":3,\"name\":\"Name_3\"}" +
                                "]"))
                );
    }

    @Test
    void testGetPortfolioByIdSuccessful() throws Exception {
        String portfolioUrl = portfoliosBaseUrl + "/2";
        mockMvc.perform(get(portfolioUrl))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"id\":2,\"name\":\"Name_2\"}")));
    }

    @Test
    void testGetPortfolioByIdNotFound() throws Exception {
        String portfolioUrl = this.portfoliosBaseUrl + "/5";
        mockMvc.perform(get(portfolioUrl))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreatePortfolioSuccessful() throws Exception {
        String portfolioBody = "{\"name\": \"Savings Account\"}";
        mockMvc.perform(post(portfoliosBaseUrl)
                .content(portfolioBody)
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    void testCreatePortfolioNameAlreadyExists() throws Exception {
        String portfolioBody = "{\"name\": \"Joint Account\"}";
        mockMvc.perform(post(portfoliosBaseUrl)
                .content(portfolioBody)
                .contentType("application/json"))
                .andExpect(status().isConflict());
    }

    @Test
    void testDeletePortfolioSuccessful() throws Exception {
        String portfolioUrl = this.portfoliosBaseUrl + "/1";
        mockMvc.perform(delete(portfolioUrl)
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void testDeletePortfolioNotFound() throws Exception {
        String portfolioUrl = this.portfoliosBaseUrl + "/5";
        mockMvc.perform(delete(portfolioUrl)
                .contentType("application/json"))
                .andExpect(status().isNotFound());
    }
}
