package com.dfse.portfoliomanager.presentation.servicesintegration;

import com.dfse.portfoliomanager.domain.entities.Portfolio;
import com.dfse.portfoliomanager.infrastructure.service.PortfolioService;
import com.dfse.portfoliomanager.infrastructure.service.PostService;
import com.dfse.portfoliomanager.presentation.controller.PortfolioController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PortfolioControllerAndService {
    @Mock
    PortfolioService portfolioService;

    @Mock
    PostService postService;

    PortfolioController portfolioController;

    @BeforeEach
    void init() {
        portfolioController = new PortfolioController(portfolioService, postService);
    }

    @Test
    public void getPortfolios_GetsCalledOnce() {
        portfolioController.getPortfolios();
        verify(portfolioService, times(1)).getPortfolios();
    }

    @Test
    public void getPortfolio_GetsCalledOnce() {
        long id = 1;
        when(portfolioService.getPortfolio(id))
                .thenReturn(new Portfolio(id, "My Portfolio", LocalDateTime.now(), LocalDateTime.now()));

        portfolioController.getPortfolio(id);
        verify(portfolioService, times(1)).getPortfolio(id);
    }
}
