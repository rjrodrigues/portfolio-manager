package com.dfse.portfoliomanager.presentation.dto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertSame;

@ExtendWith(SpringExtension.class)
class UserDtoTest {

    @Test
    public void userDtoCreation() {
        var user = new UserDto(1, "ruben.rodrigues@live.com", "Ruben", "Rodrigues");
        assertSame(user.getId(), 1L);
        assertSame(user.getEmail(), "ruben.rodrigues@live.com");
        assertSame(user.getFirstName(), "Ruben");
        assertSame(user.getLastName(), "Rodrigues");
    }
}