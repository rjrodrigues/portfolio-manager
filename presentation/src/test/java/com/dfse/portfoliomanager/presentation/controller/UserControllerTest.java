package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.presentation.controller.common.UserControllerExceptionAdvice;
import com.dfse.portfoliomanager.presentation.mockservices.MockUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {UserController.class, UserControllerExceptionAdvice.class, MockUserService.class})
@WebMvcTest
public class UserControllerTest {

    private final String usersBaseUrl = "/api/users";

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetUserByIdNotFound() throws Exception {
        String userUrl = this.usersBaseUrl + "/5";
        mockMvc.perform(get(userUrl)).andDo(print()).andExpect(status().isNotFound());
    }
}
