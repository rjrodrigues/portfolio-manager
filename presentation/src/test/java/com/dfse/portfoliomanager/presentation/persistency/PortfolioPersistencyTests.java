package com.dfse.portfoliomanager.presentation.persistency;

import com.dfse.portfoliomanager.database.repository.IPortfolioRepository;
import com.dfse.portfoliomanager.domain.entities.Portfolio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PortfolioPersistencyTests {

    @Autowired
    IPortfolioRepository portfolioRepository;

    @Test
    public void savePortfolio() {
        portfolioRepository.save(new Portfolio("Savings Account"));
        List<Portfolio> portfolios = portfolioRepository.findAll();
        assertEquals(portfolios.size(), 1);
        assertEquals(portfolios.get(0).getName(), "Savings Account");
    }

    @Test
    public void savePortfolioTwiceWithSameName() {
        portfolioRepository.save(new Portfolio("Growth"));
        assertThrows(DataIntegrityViolationException.class, () -> {
            portfolioRepository.save(new Portfolio("Growth"));
        });
    }
}
