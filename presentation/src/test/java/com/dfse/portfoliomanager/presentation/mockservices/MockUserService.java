package com.dfse.portfoliomanager.presentation.mockservices;

import com.dfse.portfoliomanager.domain.entities.User;
import com.dfse.portfoliomanager.domain.interfaces.IUserService;
import com.dfse.portfoliomanager.infrastructure.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("MockUserService")
public class MockUserService implements IUserService {
    ArrayList<User> dummyUsers = new ArrayList<>();

    @Override
    public List<User> getUsers() {
        return null;
    }

    @Override
    public User getUser(long id) {
        User result = dummyUsers
                .stream()
                .filter(userDto -> userDto.getId() == id)
                .findAny()
                .orElse(null);

        if (result != null) {
            return result;
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public long createUser(User user) {
        return 0;
    }

    @Override
    public long deleteUser(long id) {
        return 0;
    }
}
