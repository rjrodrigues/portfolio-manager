package com.dfse.portfoliomanager.presentation.controller;

import com.dfse.portfoliomanager.presentation.controller.common.AuthenticationControllerExceptionAdvice;
import com.dfse.portfoliomanager.presentation.mockservices.MockAuthenticationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {AuthenticationController.class, AuthenticationControllerExceptionAdvice.class, MockAuthenticationService.class})
@WebMvcTest
class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testLoginSuccessful() throws Exception {
        String userBody = "{\"email\": \"ruben.rodrigues@live.com\", \"password\": \"1234\"}";
        mockMvc
                .perform(post("/api/auth")
                        .content(userBody)
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void testLoginWrongUserOrPassword() throws Exception {
        String userBody = "{\"email\": \"ruben.rodrigues@live.com\", \"password\": \"12345\"}";
        mockMvc
                .perform(post("/api/auth")
                        .content(userBody)
                        .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testUserBlocked() throws Exception {
        String userBody = "{\"email\": \"blocked.user@gmail.com\", \"password\": \"1234\"}";
        mockMvc
                .perform(post("/api/auth")
                        .content(userBody)
                        .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }
}