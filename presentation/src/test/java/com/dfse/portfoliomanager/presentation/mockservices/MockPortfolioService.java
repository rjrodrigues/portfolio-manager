package com.dfse.portfoliomanager.presentation.mockservices;

import com.dfse.portfoliomanager.domain.entities.Portfolio;
import com.dfse.portfoliomanager.domain.interfaces.IPortfolioService;
import com.dfse.portfoliomanager.infrastructure.exceptions.PortfolioNameAlreadyExistsException;
import com.dfse.portfoliomanager.infrastructure.exceptions.PortfolioNotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("MockPortfolioService")
public class MockPortfolioService implements IPortfolioService {
    ArrayList<Portfolio> dummyPortfolios = new ArrayList<>();

    public MockPortfolioService() {
        for (int i = 0; i <= 3; i++) {
            Portfolio dummyPortfolio = new Portfolio((long) i, "Name_" + i, LocalDateTime.now(), LocalDateTime.now());
            dummyPortfolios.add(dummyPortfolio);
        }
    }

    @Override
    public List<Portfolio> getPortfolios() {
        return dummyPortfolios;
    }

    @Override
    public Portfolio getPortfolio(long id) {
        Portfolio result = dummyPortfolios
                .stream()
                .filter(portfolioDto -> portfolioDto.getId() == id)
                .findAny()
                .orElse(null);

        if (result != null) {
            return result;
        } else {
            throw new PortfolioNotFoundException();
        }
    }

    @Override
    public long createPortfolio(Portfolio portfolio) {
        if (portfolio.getName().equals("Joint Account")) {
            throw new PortfolioNameAlreadyExistsException();
        } else {
            return 1;
        }
    }

    @Override
    public long deletePortfolio(long id) {
        if (id == 5) {
            throw new PortfolioNotFoundException();
        } else {
            return id;
        }
    }
}
