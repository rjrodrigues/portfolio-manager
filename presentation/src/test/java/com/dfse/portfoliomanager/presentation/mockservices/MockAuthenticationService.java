package com.dfse.portfoliomanager.presentation.mockservices;

import com.dfse.portfoliomanager.domain.entities.AuthLogin;
import com.dfse.portfoliomanager.domain.interfaces.IAuthenticationService;
import com.dfse.portfoliomanager.infrastructure.exceptions.EmailOrPasswordIncorrectException;
import com.dfse.portfoliomanager.infrastructure.exceptions.UserTemporaryBlockedException;

public class MockAuthenticationService implements IAuthenticationService {
    @Override
    public String login(AuthLogin authLogin) throws EmailOrPasswordIncorrectException, UserTemporaryBlockedException {
        if (authLogin.getEmail().equals("ruben.rodrigues@live.com") &&
                authLogin.getPassword().equals("1234")) {
            return "rubensToken";
        } else if (authLogin.getEmail().equals("blocked.user@gmail.com") &&
                authLogin.getPassword().equals("1234")) {
            throw new UserTemporaryBlockedException();
        } else {
            throw new EmailOrPasswordIncorrectException();
        }
    }
}
