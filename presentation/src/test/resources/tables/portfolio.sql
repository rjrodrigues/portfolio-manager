create table portfolio
(
    id                bigint auto_increment,
    name              varchar(255)                    not null unique,
    creation_date     timestamp not null default current_timestamp,
    modification_date timestamp not null default current_timestamp
);