package com.dfse.portfoliomanager.database.repository;

import com.dfse.portfoliomanager.domain.entities.Portfolio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPortfolioRepository extends JpaRepository<Portfolio, Long> {
}
