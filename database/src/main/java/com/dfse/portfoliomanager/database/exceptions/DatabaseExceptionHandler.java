package com.dfse.portfoliomanager.database.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class DatabaseExceptionHandler {
    @AfterThrowing(pointcut = "execution(* com.dfse.portfoliomanager.database.repository.*(..))", throwing = "ex")
    protected void handleException(Exception ex) throws Throwable {
        log.warn("HERE !!!!" + ex.getMessage());
    }
}

