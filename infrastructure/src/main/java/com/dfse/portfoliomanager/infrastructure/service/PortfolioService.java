package com.dfse.portfoliomanager.infrastructure.service;

import com.dfse.portfoliomanager.database.repository.IPortfolioRepository;
import com.dfse.portfoliomanager.domain.entities.Portfolio;
import com.dfse.portfoliomanager.domain.interfaces.IPortfolioService;
import com.dfse.portfoliomanager.infrastructure.exceptions.PortfolioNameAlreadyExistsException;
import com.dfse.portfoliomanager.infrastructure.exceptions.PortfolioNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Qualifier("PortfolioService")
@Slf4j
public class PortfolioService implements IPortfolioService {

    private final IPortfolioRepository portfolioRepository;

    public PortfolioService(IPortfolioRepository portfolioRepository) {
        this.portfolioRepository = portfolioRepository;
    }

    @Override
    public List<Portfolio> getPortfolios() {
        return portfolioRepository.findAll();
    }

    @Override
    public Portfolio getPortfolio(long id) {
        return portfolioRepository.findById(id).orElseThrow(PortfolioNotFoundException::new);
    }

    @Override
    public long createPortfolio(Portfolio portfolio) {
        try {
            return portfolioRepository.saveAndFlush(portfolio).getId();
        } catch (DataIntegrityViolationException dataIntegrityViolationException) {
            throw new PortfolioNameAlreadyExistsException();
        }
    }

    @Override
    public long deletePortfolio(long id) {
        try {
            portfolioRepository.deleteById(id);
            return id;
        } catch (EmptyResultDataAccessException ex) {
            throw new PortfolioNotFoundException();
        }
    }
}
