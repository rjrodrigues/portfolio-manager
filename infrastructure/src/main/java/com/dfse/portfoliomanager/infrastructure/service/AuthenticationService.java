package com.dfse.portfoliomanager.infrastructure.service;

import com.dfse.portfoliomanager.domain.entities.AuthLogin;
import com.dfse.portfoliomanager.domain.interfaces.IAuthenticationService;
import com.dfse.portfoliomanager.infrastructure.exceptions.EmailOrPasswordIncorrectException;
import com.dfse.portfoliomanager.infrastructure.exceptions.UserTemporaryBlockedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("AuthenticationService")
public class AuthenticationService implements IAuthenticationService {
    @Override
    public String login(AuthLogin authLogin) {
        if (authLogin.getEmail().equals("ruben.rodrigues@live.com") &&
                authLogin.getPassword().equals("1234")) {
            return "rubensToken";
        } else if (authLogin.getEmail().equals("alfred.sopi@hotmail.com") &&
                authLogin.getPassword().equals("1234")) {
            return "alfredsToken";
        } else if (authLogin.getEmail().equals("blocked.user@gmail.com") &&
                authLogin.getPassword().equals("1234")) {
            throw new UserTemporaryBlockedException();
        } else {
            throw new EmailOrPasswordIncorrectException();
        }
    }
}
