package com.dfse.portfoliomanager.infrastructure.exceptions;

public class EmailOrPasswordIncorrectException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public EmailOrPasswordIncorrectException() {
        super();
    }
}

