package com.dfse.portfoliomanager.infrastructure.exceptions;

public class UserTemporaryBlockedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UserTemporaryBlockedException() {
        super();
    }
}
