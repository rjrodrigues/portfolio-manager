package com.dfse.portfoliomanager.infrastructure.service;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleStatement;
import oracle.jdbc.dcn.DatabaseChangeRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

@Service
@Qualifier("DatabaseNotifications")
public class DatabaseNotificationService {

    private final DataSource dataSource;
    private final DatabaseListener databaseListener;

    @Autowired
    public DatabaseNotificationService(DataSource dataSource, DatabaseListener databaseListener) {
        this.dataSource = dataSource;
        this.databaseListener = databaseListener;
        this.register();
    }

    private void register() {
        Properties props = new Properties();
        props.setProperty(OracleConnection.DCN_NOTIFY_ROWIDS, "true");
        props.setProperty(OracleConnection.DCN_QUERY_CHANGE_NOTIFICATION, "true");
        props.setProperty(OracleConnection.DCN_IGNORE_INSERTOP, "false");
        props.setProperty(OracleConnection.DCN_IGNORE_UPDATEOP, "false");
        props.setProperty(OracleConnection.DCN_IGNORE_DELETEOP, "false");

        try {
            OracleConnection connection = dataSource.getConnection().unwrap(OracleConnection.class);
            DatabaseChangeRegistration registration = connection.registerDatabaseChangeNotification(props);
            Statement statement = connection.createStatement();
            ((OracleStatement) statement).setDatabaseChangeRegistration(registration);
            ResultSet resultSet = statement.executeQuery("select * from db_notification");
            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String message = resultSet.getString("message");
                System.out.println("id: " + id + " message: " + message);
            }
            databaseListener.addRegistrationListener(registration);
            registration.addListener(databaseListener);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
