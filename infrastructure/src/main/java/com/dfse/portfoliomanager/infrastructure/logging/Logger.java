package com.dfse.portfoliomanager.infrastructure.logging;

import com.dfse.portfoliomanager.domain.interfaces.ILogger;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class Logger implements ILogger {

    private Class<?> callerClass;

    public Logger(Class<?> callerClass) {
        this.callerClass = callerClass;
    }

    @Override
    public void info(String message, Object object) {
        log.info(message, object.toString());
    }

    @Override
    public void debug(String message, Object object) {

    }

    @Override
    public void warn(String message, Object object) {

    }

    @Override
    public void error(String message, Object object) {

    }

    private String transformMessage(String logType, Class<?> callerClass, String message) {
        return "";
    }
}

