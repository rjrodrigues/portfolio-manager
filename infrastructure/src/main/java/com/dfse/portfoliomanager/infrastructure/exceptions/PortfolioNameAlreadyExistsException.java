package com.dfse.portfoliomanager.infrastructure.exceptions;

public class PortfolioNameAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PortfolioNameAlreadyExistsException() {
        super();
    }
}