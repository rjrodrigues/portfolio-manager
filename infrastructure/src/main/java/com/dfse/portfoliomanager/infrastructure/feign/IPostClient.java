package com.dfse.portfoliomanager.infrastructure.feign;

import com.dfse.portfoliomanager.domain.entities.Post;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "jplaceholder", url = "https://jsonplaceholder.typicode.com/")
public interface IPostClient {
    @RequestMapping(method = RequestMethod.GET, value = "/posts")
    List<Post> getPosts();
}
