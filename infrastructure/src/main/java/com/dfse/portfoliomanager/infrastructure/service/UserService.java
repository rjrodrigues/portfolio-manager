package com.dfse.portfoliomanager.infrastructure.service;

import com.dfse.portfoliomanager.domain.entities.User;
import com.dfse.portfoliomanager.domain.interfaces.IUserService;
import com.dfse.portfoliomanager.infrastructure.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("UserService")
public class UserService implements IUserService {

    @Override
    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User(1, "ruben.rodrigues@live.com", "Ruben", "Rodrigues", LocalDateTime.now(), LocalDateTime.now()));
        return users;
    }

    @Override
    public User getUser(long id) {
        return new User(1, "ruben.rodrigues@live.com", "Ruben", "Rodrigues", LocalDateTime.now(), LocalDateTime.now());
    }

    @Override
    public long createUser(User user) {
        return 0;
    }

    @Override
    public long deleteUser(long id) {
        throw new UserNotFoundException();
    }
}
