package com.dfse.portfoliomanager.infrastructure.service;

import oracle.jdbc.dcn.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("DatabaseListener")
public class DatabaseListener implements DatabaseChangeListener {

    private DatabaseChangeRegistration registration;

    public void addRegistrationListener(DatabaseChangeRegistration registration) {
        this.registration = registration;
    }

    @Override
    public void onDatabaseChangeNotification(DatabaseChangeEvent databaseChangeEvent) {
        System.out.println("onDatabaseChangeNotification");
        if (databaseChangeEvent.getRegId() == registration.getRegId()) {
            System.out.println(databaseChangeEvent.toString());
            QueryChangeDescription[] queryChanges = databaseChangeEvent.getQueryChangeDescription();
            TableChangeDescription[] tableChanges = databaseChangeEvent.getTableChangeDescription();
            if (tableChanges != null) {
                for (TableChangeDescription tableChange : tableChanges) {
                    RowChangeDescription[] changes = tableChange.getRowChangeDescription();
                    for (RowChangeDescription change : changes) {
                        RowChangeDescription.RowOperation op = change.getRowOperation();
                        String rowId = change.getRowid().stringValue();
                        System.out.println("rowId: " + rowId);
                        switch (op) {
                            case INSERT:
                                //process
                                System.out.println("inserted");
                                break;
                            case UPDATE:
                                //do nothing
                                System.out.println("updated");
                                break;
                            case DELETE:
                                System.out.println("deleted");
                                //do nothing
                                break;
                            default:
                                System.out.println("default");
                                //do nothing
                        }
                    }
                }
            }
        }
    }
}
