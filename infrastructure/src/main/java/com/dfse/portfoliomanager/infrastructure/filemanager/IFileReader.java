package com.dfse.portfoliomanager.infrastructure.filemanager;

public interface IFileReader {
    public void read();
}
