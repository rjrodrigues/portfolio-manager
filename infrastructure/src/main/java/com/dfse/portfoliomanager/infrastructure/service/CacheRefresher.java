package com.dfse.portfoliomanager.infrastructure.service;

import com.dfse.portfoliomanager.domain.interfaces.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CacheRefresher {

    private final IPostService postService;

    @Autowired
    public CacheRefresher(IPostService postService) {
        this.postService = postService;
    }

    @Scheduled(fixedRate = 50000)
    public void refreshCaches() {
        postService.refreshCache();
    }
}
