package com.dfse.portfoliomanager.infrastructure.filemanager;

public interface IFileWriter {
    public void write();
}
