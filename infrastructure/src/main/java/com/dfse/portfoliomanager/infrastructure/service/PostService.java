package com.dfse.portfoliomanager.infrastructure.service;

import com.dfse.portfoliomanager.domain.entities.Post;
import com.dfse.portfoliomanager.domain.interfaces.IPostService;
import com.dfse.portfoliomanager.infrastructure.feign.IPostClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Qualifier("PostService")
@Slf4j
public class PostService implements IPostService {

    private final IPostClient postClient;

    public PostService(IPostClient postClient) {
        this.postClient = postClient;
    }

    @Override
    @Cacheable("posts")
    public List<Post> getPosts() {
        return postClient.getPosts();
    }

    @Override
    @CachePut("posts")
    public List<Post> refreshCache() {
        return postClient.getPosts();
    }
}
