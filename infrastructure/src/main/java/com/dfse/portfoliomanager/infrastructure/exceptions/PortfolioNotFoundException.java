package com.dfse.portfoliomanager.infrastructure.exceptions;

public class PortfolioNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PortfolioNotFoundException() {
        super();
    }
}
